import cheerio from 'cheerio'
import https from 'https'

const MEPS_LIST_URL = 'https://www.europarl.europa.eu/meps/en/full-list/all'

/**
 * Fetching functions
 */

function fetchWithPromise(url) {
	return new Promise((resolve, reject) => {
		https.get(url, (response) => {
			let chunks = []

			response.on('data', (fragments) => {
				chunks.push(fragments)
			})

			response.on('end', () => {
        let responseBody = Buffer.concat(chunks)
				resolve(responseBody.toString())
			})

			response.on('error', (error) => {
				reject(error)
			})
		})
	})
}

// async function to make http request
async function makeSynchronousRequest(url) {
	try {
		const promise = fetchWithPromise(url)
		const body = await promise

		// holds response from server that is passed when Promise is resolved
		return body
	}
	catch(error) {
		// Promise rejected
		console.log(error)
	}
}

/**
 * Utils Functions
 */

const hasLowerCase = (str) => {
  return (/[a-z]/.test(str))
}

const getLastName = (fullName) => {
  return fullName.split(' ').reduce((prev, curr) => {
    return !hasLowerCase(curr)
      ?
        prev.length
          ? prev += ` ${curr}`
          : curr
      : prev
  }, '')
}

const parseElement = (el) => {
  const name = el.find('.erpl_title-h5').text()
  const lastName = getLastName(name)
  const imageNode = el.find('.erpl_image-frame').find('span')
  const image = /\'(.+)\'/.exec(imageNode.attr('style'))[1].replace('https:', 'http:')
  const details = el.find('.erpl_member-list-item-content').closest('div')
  const partyGroup = details.find('.mb-25').first().text()
  const country = details.find('.mb-25').first().next().text()
  const baseUrl = el.find('.erpl_member-list-item-content').attr('href')
  const nameBits = [...name.split(' ').map(str => str.toUpperCase())]
  const url = `${baseUrl}/${nameBits[0]}_${nameBits.slice(1).join('+')}/home`

  return {
    name,
    lastName,
    partyGroup,
    url,
    country,
    image,
  }
}

export const scrapeEULegislators = async () => {
  const html = await makeSynchronousRequest(MEPS_LIST_URL)
  const $ = cheerio.load(html)

  const meps = $('.erpl_member-list-item').map((_, el) => parseElement($(el)))

  return meps.get()
}
